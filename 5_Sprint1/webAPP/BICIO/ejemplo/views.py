from django.shortcuts import render
from django.views.generic import TemplateView
from suds.client import Client
#import xml.etree.ElementTree as ET
#from xml.dom import minidom
from suds.sax.element import Element

class ModelWebService:
	def __init__(self,var):
		self.var = var

	def holamundo(self,variable):
		return variable

class llamadaEjemplos(TemplateView):

	def __init__(self,msj1,msj2):
		self.msj1A = msj1
		self.msj2A = msj2

	def llamadaEjemplo_1(self, request):
		#NO usar request porque se repite la variable
		#consumir servicioweb
		client =  Client(url='http://www.webservicex.com/globalweather.asmx?WSDL')
		print client
		CountryName = 'Chile' 
		CityName = 'Santiago'
		response_suds = client.service.GetWeather(CityName, CountryName)
		#response_suds = client.service.GetCitiesByCountry(CountryName)
		#Respuesta web service en xml
		print response_suds
		#response_str = response_suds
		#response_suds = "Descomentar consumo WS"
		context = {'msje': 'Consumiento un web service con SUDS : http://www.webservicex.com/globalweather.asmx?WSDL','msje2':response_suds}
		return render(request,'ejemplo/template_1.html',context)

	def llamadaEjemplo_2(self,request):
		context = {'msje': self.msj1A,'msje2': self.msj2A}
		return render(request,'ejemplo/template_2.html', context)
	

class Graficos(TemplateView):

	def llamadaGrafico_1(self,request):
		return render(request,'')