from django.conf.urls import patterns, include, url
from django.contrib import admin
#from ejemplo import views
from .views import llamadaEjemplos

#llamadaEjemplos_ = llamadaEjemplos('Hola','Mundo')
#llamadaEjemplos = llamadaEjemplos.as_view()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BICIO.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

	url(r'^llamadaEjemplo_1/$', llamadaEjemplos('msj1','msj2').llamadaEjemplo_1),
	url(r'^llamadaEjemplos/$', llamadaEjemplos('Hola','Mundo').llamadaEjemplo_2),
)