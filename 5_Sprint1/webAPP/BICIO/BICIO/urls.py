from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'BICIO.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'BICIO.views.home', name='home'),

    url(r'^BICIO/', include('BICIO.urls')),
    url(r'^ejemplo/', include('ejemplo.urls')),
)
